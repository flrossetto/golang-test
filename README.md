# API

Run projeto
```bash
docker-compose up -d
go run main.go
```

Run test
```bash
# Install mockgen
go install github.com/golang/mock/mockgen@v1.6.0

# create mocks
go generate ./...

# run
go test ./...
```

Requests

```bash
# find
curl --request GET --url http://127.0.0.1:3000/planets

# retrieve
curl --request GET --url http://127.0.0.1:3000/planets/[id]

# creare
curl --request POST --url http://127.0.0.1:3000/planets --header 'Content-Type: application/json' --data '{"code": 1}'

# delete
curl --request DELETE --url http://127.0.0.1:3000/planets/[id]
```
