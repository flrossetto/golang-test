package main

import "github.com/star-wars/api/pkg/server"

func main() {
	server.Run()
}
