package entity

import (
	"github.com/google/uuid"
)

type Planet struct {
	ID         uuid.UUID `json:"id,omitempty" bson:"_id,omitempty"`
	Code       int64
	Name       string
	Climate    string
	Terrain    string
	FilmsCount int64
}
