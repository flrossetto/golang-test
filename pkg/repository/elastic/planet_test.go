package elastic

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/elastic/go-elasticsearch/esapi"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/google/uuid"
	"github.com/star-wars/api/pkg/core"
	"github.com/star-wars/api/pkg/repository/entity"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

var elasticClient *elasticsearch.Client

const indexnameForTest = "index_test"

func TestMain(m *testing.M) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	req := testcontainers.ContainerRequest{
		Image:        "docker.io/bitnami/elasticsearch:7",
		ExposedPorts: []string{"9200/tcp"},
		WaitingFor:   wait.ForListeningPort("9200"),
	}
	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer container.Terminate(ctx)

	ip, err := container.Host(ctx)
	if err != nil {
		log.Fatal(err)
	}
	mappedPort, err := container.MappedPort(ctx, "9200")
	if err != nil {
		log.Fatal(err)
	}

	cfg := elasticsearch.Config{
		Addresses: []string{
			fmt.Sprintf("http://%s:%s", ip, mappedPort.Port()),
		},
	}

	elasticClient, err = elasticsearch.NewClient(cfg)
	if err != nil {
		log.Fatal(err)
	}

	os.Exit(m.Run())
}

func dropIndex(indexName string) {
	req := esapi.IndicesDeleteRequest{
		Index: []string{indexName},
	}
	req.Do(context.Background(), elasticClient)
}

func TestNewPlanetCollection(t *testing.T) {
	collection := NewPlanetCollection(elasticClient, "indexName")
	assert.Equal(t, "indexName", collection.indexName)
	assert.Equal(t, elasticClient, collection.esClient)
}

func TestPlanetCollection_ErrDuplicatedPlanet(t *testing.T) {
	defer dropIndex(indexnameForTest)

	repo := NewPlanetCollection(elasticClient, indexnameForTest)

	err := repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 1, Name: "item"})
	assert.Nil(t, err)

	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 2, Name: "item"})
	assert.Nil(t, err)

	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 1, Name: "item"})
	assert.ErrorIs(t, err, core.ErrDuplicatedPlanet)
}

func TestPlanetCollection_CRD(t *testing.T) {
	defer dropIndex(indexnameForTest)

	repo := NewPlanetCollection(elasticClient, indexnameForTest)

	// --- create ---
	err := repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 15, Name: "item 15"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 25, Name: "item 25 - teste query"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 35, Name: "item 35"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 45, Name: "item 45"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 55, Name: "item 55 - teste query"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 65, Name: "item 65"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 75, Name: "item 75 - teste query"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 85, Name: "item 85"})
	assert.Nil(t, err)

	// --- Read ---
	result, err := repo.Find(context.Background(), 5, "")
	assert.Nil(t, err)
	assert.Len(t, result, 0)

	result, err = repo.Find(context.Background(), 0, "")
	assert.Nil(t, err)
	assert.Len(t, result, 8)

	result, err = repo.Find(context.Background(), 35, "")
	assert.Nil(t, err)
	assert.Len(t, result, 1)

	result, err = repo.Find(context.Background(), 0, "teste")
	assert.Nil(t, err)
	assert.Len(t, result, 3)

	result, err = repo.Find(context.Background(), 55, "teste")
	assert.Nil(t, err)
	assert.Len(t, result, 1)

	result, err = repo.Find(context.Background(), 85, "")
	assert.Nil(t, err)
	assert.Len(t, result, 1)
	planet := result[0]

	planetFronRetrieve, err := repo.Retrieve(context.Background(), planet.ID)
	assert.Nil(t, err)
	assert.Equal(t, &entity.Planet{
		ID:   planet.ID,
		Code: 85,
		Name: "item 85",
	}, planetFronRetrieve)

	// --- Delete ---
	err = repo.Delete(context.Background(), planet.ID)
	assert.Nil(t, err)

	// espera es apagar
	time.Sleep(2 * time.Second)

	result, err = repo.Find(context.Background(), 0, "")
	assert.Nil(t, err)
	assert.Len(t, result, 7)
}

// func TestPlanetCollection_InvalidDocument(t *testing.T) {
// 	defer dropIndex(indexnameForTest)

// 	id := uuid.New()

// 	repo := NewPlanetCollection(elasticClient, indexnameForTest)
// 	repo.collection().InsertOne(context.Background(), bson.D{
// 		{"_id", id},
// 		{"code", "invalid_value"},
// 	})

// 	findResult, err := repo.Find(context.Background(), 0, "")
// 	assert.NotNil(t, err)
// 	assert.Nil(t, findResult)

// 	retrieveResult, err := repo.Retrieve(context.Background(), id)
// 	assert.NotNil(t, err)
// 	assert.Nil(t, retrieveResult)
// }

// func TestPlanetCollection_FindError(t *testing.T) {
// 	defer dropIndex(indexnameForTest)

// 	id := uuid.New()

// 	repo := NewPlanetCollection(elasticClient, indexnameForTest)
// 	repo.collection().InsertOne(context.Background(), bson.D{
// 		{"_id", id},
// 		{"code", 1},
// 	})

// 	ctx, cancel := context.WithCancel(context.Background())
// 	cancel() // force error

// 	findResult, err := repo.Find(ctx, 0, "")
// 	assert.NotNil(t, err)
// 	assert.Nil(t, findResult)
// }
