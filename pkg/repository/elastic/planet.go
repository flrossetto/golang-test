package elastic

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/elastic/go-elasticsearch/esapi"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/google/uuid"
	"github.com/star-wars/api/pkg/core"
	"github.com/star-wars/api/pkg/repository/entity"
)

type PlanetCollection struct {
	esClient  *elasticsearch.Client
	indexName string
}

func NewPlanetCollection(esClient *elasticsearch.Client, indexName string) *PlanetCollection {
	return &PlanetCollection{
		esClient:  esClient,
		indexName: indexName,
	}
}

func (c *PlanetCollection) Find(ctx context.Context, code int64, name string) ([]*entity.Planet, error) {
	var filter []string

	if name != "" {
		filter = append(filter, fmt.Sprintf("Name: *%s*", name))
	}

	if code > 0 {
		filter = append(filter, fmt.Sprintf("Code: %d", code))
	}

	query := make(map[string]interface{})

	if len(filter) > 0 {
		query["query"] = map[string]interface{}{
			"query_string": map[string]interface{}{
				"query": strings.Join(filter, " AND "),
			},
		}
	}

	var buffer bytes.Buffer
	json.NewEncoder(&buffer).Encode(&query)

	res, err := c.esClient.Search(
		c.esClient.Search.WithContext(ctx),
		c.esClient.Search.WithBody(&buffer),
		c.esClient.Search.WithIndex(c.indexName),
		c.esClient.Search.WithPretty(),
	)

	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, fmt.Errorf("elasticsearch: find document: %s", res.Status())
	}

	var searchResult struct {
		Hits struct {
			Hits []struct {
				Source *entity.Planet `json:"_source"`
			} `json:"hits"`
		} `json:"hits"`
	}

	if err := json.NewDecoder(res.Body).Decode(&searchResult); err != nil {
		return nil, err
	}

	result := make([]*entity.Planet, 0, len(searchResult.Hits.Hits))
	for _, planet := range searchResult.Hits.Hits {
		result = append(result, planet.Source)
	}

	return result, nil
}

func (c *PlanetCollection) Retrieve(ctx context.Context, id uuid.UUID) (*entity.Planet, error) {
	res, err := c.esClient.Get(c.indexName, id.String())
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	if res.IsError() {
		return nil, fmt.Errorf("elasticsearch: find document: %s", res.Status())
	}

	var getResult struct {
		Source *entity.Planet `json:"_source"`
	}

	if err := json.NewDecoder(res.Body).Decode(&getResult); err != nil {
		return nil, err
	}

	return getResult.Source, nil
}

func (c *PlanetCollection) Create(ctx context.Context, planet entity.Planet) error {
	list, err := c.Find(ctx, planet.Code, "")
	if err == nil && len(list) > 0 {
		return core.ErrDuplicatedPlanet
	}

	body, err := json.Marshal(planet)
	if err != nil {
		return err
	}

	req := esapi.IndexRequest{
		Index:      c.indexName,
		DocumentID: planet.ID.String(),
		Body:       bytes.NewReader(body),
		Refresh:    "true",
	}

	res, err := req.Do(ctx, c.esClient)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	if res.IsError() {
		return fmt.Errorf("elasticsearch: create document: %s", res.Status())
	}

	return nil
}

func (c *PlanetCollection) Delete(ctx context.Context, id uuid.UUID) error {
	res, err := c.esClient.Delete(c.indexName, id.String())
	if err != nil {
		return err
	}

	defer res.Body.Close()

	if res.IsError() {
		return fmt.Errorf("elasticsearch: find document: %s", res.Status())
	}

	return nil
}
