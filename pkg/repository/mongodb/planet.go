package mongodb

import (
	"context"

	"github.com/star-wars/api/pkg/core"
	"github.com/star-wars/api/pkg/repository/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/google/uuid"
)

type PlanetCollection struct {
	mongoClient  *mongo.Client
	databaseName string
}

func NewPlanetCollection(mongoClient *mongo.Client, databaseName string) *PlanetCollection {
	return &PlanetCollection{
		mongoClient:  mongoClient,
		databaseName: databaseName,
	}
}

func (c *PlanetCollection) collection() *mongo.Collection {
	return c.mongoClient.
		Database(c.databaseName).
		Collection("planets")
}

func (c *PlanetCollection) Find(ctx context.Context, code int64, name string) ([]*entity.Planet, error) {
	var orArgs bson.A

	if code > 0 {
		orArgs = append(orArgs, bson.M{"code": code})
	}

	if name != "" {
		orArgs = append(orArgs, bson.M{"name": bson.M{"$regex": name}})
	}

	var query bson.M

	if len(orArgs) == 1 {
		query = orArgs[0].(bson.M)
	}
	if len(orArgs) > 1 {
		query = bson.M{"$and": orArgs}
	}

	cursor, err := c.collection().Find(ctx, query)
	if err != nil {
		return nil, err
	}

	var planets []*entity.Planet
	if err := cursor.All(ctx, &planets); err != nil {
		return nil, err
	}

	return planets, nil
}

func (c *PlanetCollection) Retrieve(ctx context.Context, id uuid.UUID) (*entity.Planet, error) {
	result := c.collection().FindOne(ctx, bson.M{"_id": id})

	var planets *entity.Planet
	if err := result.Decode(&planets); err != nil {
		return nil, err
	}

	return planets, nil
}

func (c *PlanetCollection) Create(ctx context.Context, planet entity.Planet) error {
	count, err := c.collection().CountDocuments(ctx, bson.M{"code": planet.Code})
	if err != nil {
		return err
	}
	if count > 0 {
		return core.ErrDuplicatedPlanet
	}

	_, err = c.collection().InsertOne(ctx, planet)
	return err
}

func (c *PlanetCollection) Delete(ctx context.Context, id uuid.UUID) error {
	_, err := c.collection().DeleteOne(ctx, bson.M{"_id": id})
	return err
}
