package mongodb

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/star-wars/api/pkg/core"
	"github.com/star-wars/api/pkg/repository/entity"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mongoClient *mongo.Client

const databaseNameForTest = "db_test"

func TestMain(m *testing.M) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	req := testcontainers.ContainerRequest{
		Image:        "mongo",
		ExposedPorts: []string{"27017/tcp"},
		WaitingFor:   wait.ForListeningPort("27017"),
	}
	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer container.Terminate(ctx)

	ip, err := container.Host(ctx)
	if err != nil {
		log.Fatal(err)
	}
	mappedPort, err := container.MappedPort(ctx, "27017")
	if err != nil {
		log.Fatal(err)
	}

	mongoClient, err = mongo.Connect(
		ctx,
		options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", ip, mappedPort.Port())),
	)
	if err != nil {
		log.Fatal(err)
	}

	defer mongoClient.Disconnect(ctx)

	os.Exit(m.Run())
}

func TestNewPlanetCollection(t *testing.T) {
	collection := NewPlanetCollection(mongoClient, "databaseName")
	assert.Equal(t, "databaseName", collection.databaseName)
	assert.Equal(t, mongoClient, collection.mongoClient)
}

func TestPlanetCollection_ErrDuplicatedPlanet(t *testing.T) {
	defer mongoClient.Database(databaseNameForTest).Drop(context.Background())

	repo := NewPlanetCollection(mongoClient, databaseNameForTest)

	err := repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 1, Name: "item"})
	assert.Nil(t, err)

	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 2, Name: "item"})
	assert.Nil(t, err)

	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 1, Name: "item"})
	assert.ErrorIs(t, err, core.ErrDuplicatedPlanet)
}

func TestPlanetCollection_CRD(t *testing.T) {
	defer mongoClient.Database(databaseNameForTest).Drop(context.Background())

	repo := NewPlanetCollection(mongoClient, databaseNameForTest)

	// --- create ---

	err := repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 15, Name: "item 15"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 25, Name: "item 25 - teste query"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 35, Name: "item 35"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 45, Name: "item 45"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 55, Name: "item 55 - teste query"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 65, Name: "item 65"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 75, Name: "item 75 - teste query"})
	assert.Nil(t, err)
	err = repo.Create(context.Background(), entity.Planet{ID: uuid.New(), Code: 85, Name: "item 85"})
	assert.Nil(t, err)

	// --- Read ---
	result, err := repo.Find(context.Background(), 5, "")
	assert.Nil(t, err)
	assert.Len(t, result, 0)

	result, err = repo.Find(context.Background(), 0, "")
	assert.Nil(t, err)
	assert.Len(t, result, 8)

	result, err = repo.Find(context.Background(), 35, "")
	assert.Nil(t, err)
	assert.Len(t, result, 1)

	result, err = repo.Find(context.Background(), 0, "item 4")
	assert.Nil(t, err)
	assert.Len(t, result, 1)

	result, err = repo.Find(context.Background(), 0, "teste")
	assert.Nil(t, err)
	assert.Len(t, result, 3)

	result, err = repo.Find(context.Background(), 55, "teste")
	assert.Nil(t, err)
	assert.Len(t, result, 1)

	result, err = repo.Find(context.Background(), 85, "")
	assert.Nil(t, err)
	assert.Len(t, result, 1)
	planet := result[0]

	planetFronRetrieve, err := repo.Retrieve(context.Background(), planet.ID)
	assert.Nil(t, err)
	assert.Equal(t, &entity.Planet{
		ID:   planet.ID,
		Code: 85,
		Name: "item 85",
	}, planetFronRetrieve)

	// --- Delete ---

	err = repo.Delete(context.Background(), planet.ID)
	assert.Nil(t, err)

	result, err = repo.Find(context.Background(), 0, "")
	assert.Nil(t, err)
	assert.Len(t, result, 7)
}

func TestPlanetCollection_InvalidDocument(t *testing.T) {
	defer mongoClient.Database(databaseNameForTest).Drop(context.Background())

	id := uuid.New()

	repo := NewPlanetCollection(mongoClient, databaseNameForTest)
	repo.collection().InsertOne(context.Background(), bson.D{
		{"_id", id},
		{"code", "invalid_value"},
	})

	findResult, err := repo.Find(context.Background(), 0, "")
	assert.NotNil(t, err)
	assert.Nil(t, findResult)

	retrieveResult, err := repo.Retrieve(context.Background(), id)
	assert.NotNil(t, err)
	assert.Nil(t, retrieveResult)
}

func TestPlanetCollection_FindError(t *testing.T) {
	defer mongoClient.Database(databaseNameForTest).Drop(context.Background())

	id := uuid.New()

	repo := NewPlanetCollection(mongoClient, databaseNameForTest)
	repo.collection().InsertOne(context.Background(), bson.D{
		{"_id", id},
		{"code", 1},
	})

	ctx, cancel := context.WithCancel(context.Background())
	cancel() // force error

	findResult, err := repo.Find(ctx, 0, "")
	assert.NotNil(t, err)
	assert.Nil(t, findResult)
}
