package repository

import (
	"context"

	"github.com/star-wars/api/pkg/repository/entity"

	"github.com/google/uuid"
)

type FindPlanet interface {
	Find(ctx context.Context, code int64, name string) ([]*entity.Planet, error)
}

type RetrievePlanet interface {
	Retrieve(ctx context.Context, id uuid.UUID) (*entity.Planet, error)
}

type CreatePlanet interface {
	Create(context.Context, entity.Planet) error
}

type DeletePlanet interface {
	Delete(ctx context.Context, id uuid.UUID) error
}

//go:generate mockgen -destination=../../mocks/repositorytest/planetrepository.go -package=repositorytest github.com/star-wars/api/pkg/repository PlanetRepository
type PlanetRepository interface {
	FindPlanet
	RetrievePlanet
	CreatePlanet
	DeletePlanet
}
