package starwarsapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/star-wars/api/pkg/repository/entity"
)

type PlanetInfo struct {
	Name           string   `json:"name"`
	RotationPeriod string   `json:"rotation_period"`
	OrbitalPeriod  string   `json:"orbital_period"`
	Diameter       string   `json:"diameter"`
	Climate        string   `json:"climate"`
	Gravity        string   `json:"gravity"`
	Terrain        string   `json:"terrain"`
	SurfaceWater   string   `json:"surface_water"`
	Population     string   `json:"population"`
	Residents      []string `json:"residents"`
	Films          []string `json:"films"`
	Created        string   `json:"created"`
	Edited         string   `json:"edited"`
	URL            string   `json:"url"`
}

type StarWarsAPI struct {
}

func New() *StarWarsAPI {
	return &StarWarsAPI{}
}

func (api *StarWarsAPI) LoadPlanetDetails(planet *entity.Planet) error {
	res, err := http.Get(fmt.Sprintf("https://swapi.dev/api/planets/%d", planet.Code))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("invalid planet")
	}

	defer res.Body.Close()

	var planetInfo PlanetInfo
	if err := json.NewDecoder(res.Body).Decode(&planetInfo); err != nil {
		return err
	}

	planet.Name = planetInfo.Name
	planet.Climate = planetInfo.Climate
	planet.Terrain = planetInfo.Terrain
	planet.FilmsCount = int64(len(planetInfo.Films))

	return nil
}
