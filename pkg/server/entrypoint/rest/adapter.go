package rest

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/star-wars/api/pkg/core"
)

//go:generate mockgen -destination=../../../../mocks/resttest/planetservice.go -package=resttest github.com/star-wars/api/pkg/server/entrypoint/rest PlanetService
type PlanetService interface {
	Retrieve(ctx context.Context, id uuid.UUID) (*core.PlanetRepresentation, error)
	Find(ctx context.Context, intent core.FindPlanetIntent) ([]core.PlanetFindRepresentation, error)
	Create(ctx context.Context, intent core.CreatePlanetIntent) error
	Delete(ctx context.Context, id uuid.UUID) error
}

//go:generate mockgen -destination=../../../../mocks/resttest/router.go -package=resttest github.com/star-wars/api/pkg/server/entrypoint/rest Router
type Router interface {
	Get(path string, handlers ...fiber.Handler) fiber.Router
	Post(path string, handlers ...fiber.Handler) fiber.Router
	Put(path string, handlers ...fiber.Handler) fiber.Router
	Delete(path string, handlers ...fiber.Handler) fiber.Router
	Patch(path string, handlers ...fiber.Handler) fiber.Router
}
