package rest

import (
	"errors"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/star-wars/api/pkg/core"
)

type (
	CreatePlanetIntent struct {
		Code int64 `json:"code"`
	}

	PlanetFindRepresentation struct {
		ID   uuid.UUID `json:"id"`
		Code int64     `json:"code"`
		Name string    `json:"name"`
	}

	PlanetRepresentation struct {
		ID         uuid.UUID `json:"id"`
		Code       int64     `json:"code"`
		Name       string    `json:"name"`
		Climate    string    `json:"climate"`
		Terrain    string    `json:"terrain"`
		FilmsCount int64     `json:"films_count"`
	}
)

type PlanetController struct {
	router        Router
	planetService PlanetService
}

func NewPlanetController(router Router, planetService PlanetService) *PlanetController {
	api := &PlanetController{
		router:        router,
		planetService: planetService,
	}

	api.router.Get("/planets", api.findPlanets)
	api.router.Post("/planets", api.createPlanet)
	api.router.Get("/planets/:id", api.retrievePlanet)
	api.router.Delete("/planets/:id", api.deletePlanet)

	return api
}

func (api *PlanetController) findPlanets(c *fiber.Ctx) error {
	code, _ := strconv.ParseInt(c.Query("code"), 10, 64)

	planets, err := api.planetService.Find(c.Context(), core.FindPlanetIntent{
		Code: int64(code),
		Name: c.Query("name"),
	})
	if err != nil {
		return errorHandle(c, err)
	}

	result := make([]PlanetFindRepresentation, 0, len(planets))
	for _, planet := range planets {
		result = append(result, PlanetFindRepresentation(planet))
	}

	return c.JSON(result)
}

func (api *PlanetController) retrievePlanet(c *fiber.Ctx) error {
	planet, err := api.planetService.Retrieve(c.Context(), uuid.MustParse(c.Params("id")))
	if err != nil {
		return errorHandle(c, err)
	}

	return c.JSON((*PlanetRepresentation)(planet))

}

func (api *PlanetController) createPlanet(c *fiber.Ctx) error {
	var intent CreatePlanetIntent

	if err := c.BodyParser(&intent); err != nil {
		return errorHandle(c, err)
	}

	err := api.planetService.Create(c.Context(), core.CreatePlanetIntent(intent))
	if err != nil {
		return errorHandle(c, err)
	}

	return c.SendStatus(fiber.StatusCreated)
}

func (api *PlanetController) deletePlanet(c *fiber.Ctx) error {
	err := api.planetService.Delete(c.Context(), uuid.MustParse(c.Params("id")))
	if err != nil {
		return errorHandle(c, err)
	}

	return c.SendStatus(fiber.StatusNoContent)
}

func errorHandle(c *fiber.Ctx, err error) error {
	if fiberError, ok := err.(*fiber.Error); ok {
		return c.Status(fiberError.Code).JSON(fiberError)
	}

	if errors.Is(err, core.ErrPlanetNotFound) {
		return c.SendStatus(fiber.StatusNotFound)
	}

	if errs, ok := err.(*core.ValidationErrors); ok {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errs)
	}

	return c.Status(fiber.StatusInternalServerError).JSON(map[string]string{"error": err.Error()})
}
