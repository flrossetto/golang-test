package rest

import (
	"errors"
	"io/ioutil"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/star-wars/api/mocks/resttest"
	"github.com/star-wars/api/pkg/core"
	"github.com/stretchr/testify/assert"
)

func TestAPI_Start(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	router := resttest.NewMockRouter(ctrl)

	router.EXPECT().Get("/planets", gomock.Any())
	router.EXPECT().Post("/planets", gomock.Any())
	router.EXPECT().Get("/planets/:id", gomock.Any())
	router.EXPECT().Delete("/planets/:id", gomock.Any())

	NewPlanetController(router, nil)
}

func TestAPI_findPlanets(t *testing.T) {
	t.Run("not found", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Find(gomock.Any(), gomock.Eq(core.FindPlanetIntent{Code: 1, Name: "teste"})).
			Return(nil, core.ErrPlanetNotFound)

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/planets?code=1&name=teste", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusNotFound, resp.StatusCode)
	})

	t.Run("error when running find in service", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Find(gomock.Any(), gomock.Eq(core.FindPlanetIntent{Code: 1, Name: "teste"})).
			Return(nil, errors.New("internal server error"))

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/planets?code=1&name=teste", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusInternalServerError, resp.StatusCode)
	})

	t.Run("success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		result := []core.PlanetFindRepresentation{
			{
				ID:   uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"),
				Code: 1,
				Name: "Name 1",
			},
			{
				ID:   uuid.MustParse("d65d8637-d161-4ed1-9696-3c2bead9612f"),
				Code: 1,
				Name: "Name 2",
			},
		}

		planetService.EXPECT().
			Find(gomock.Any(), gomock.Eq(core.FindPlanetIntent{Code: 1, Name: "teste"})).
			Return(result, nil)

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/planets?code=1&name=teste", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusOK, resp.StatusCode)

		expected := `[
			{
				"id":"6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b",
				"code": 1,
				"name":"Name 1"
			}, {
				"id":"d65d8637-d161-4ed1-9696-3c2bead9612f",
				"code": 1,
				"name":"Name 2"
			}
		]`

		body, err := ioutil.ReadAll(resp.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, expected, string(body))
	})
}

func TestAPI_retrievePlanet(t *testing.T) {
	t.Run("not found", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))).
			Return(nil, core.ErrPlanetNotFound)

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/planets/6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusNotFound, resp.StatusCode)
	})

	t.Run("internal server error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))).
			Return(nil, errors.New("internal server error"))

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/planets/6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusInternalServerError, resp.StatusCode)
	})

	t.Run("success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		result := &core.PlanetRepresentation{
			ID:         uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"),
			Code:       1,
			Name:       "Name",
			Climate:    "Climate",
			Terrain:    "Terrain",
			FilmsCount: 10,
		}

		planetService.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))).
			Return(result, nil)

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/planets/6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusOK, resp.StatusCode)

		expected := `{
			"id": "6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b",
			"code": 1,
			"name": "Name",
			"terrain": "Terrain",
			"climate": "Climate",
			"films_count": 10
		}`

		body, err := ioutil.ReadAll(resp.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, expected, string(body))
	})
}

func TestAPI_createPlanet(t *testing.T) {
	t.Run("unprocessable entity", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		NewPlanetController(router, planetService)

		payload := `{123}`

		resp, err := router.Test(httptest.NewRequest(fiber.MethodPost, "/planets", strings.NewReader(payload)))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusUnprocessableEntity, resp.StatusCode)
	})

	t.Run("error when running create in service", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		intent := core.CreatePlanetIntent{
			Code: 1,
		}

		planetService.EXPECT().
			Create(gomock.Any(), gomock.Eq(intent)).
			Return(errors.New("service error"))

		NewPlanetController(router, planetService)

		payload := `{ "code": 1 }`

		request := httptest.NewRequest(fiber.MethodPost, "/planets", strings.NewReader(payload))
		request.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

		resp, err := router.Test(request)
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusInternalServerError, resp.StatusCode)
	})

	t.Run("planet created", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		intent := core.CreatePlanetIntent{
			Code: 1,
		}

		planetService.EXPECT().
			Create(gomock.Any(), gomock.Eq(intent)).
			Return(nil)

		NewPlanetController(router, planetService)

		payload := `{
			"code": 1
		}`

		request := httptest.NewRequest(fiber.MethodPost, "/planets", strings.NewReader(payload))
		request.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)

		resp, err := router.Test(request)
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusCreated, resp.StatusCode)
	})
}

func TestAPI_deletePlanet(t *testing.T) {
	t.Run("not found", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Delete(gomock.Any(), gomock.Eq(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))).
			Return(core.ErrPlanetNotFound)

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodDelete, "/planets/6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusNotFound, resp.StatusCode)
	})

	t.Run("error when runnig delete in service", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Delete(gomock.Any(), gomock.Eq(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))).
			Return(errors.New("internal server error"))

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodDelete, "/planets/6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusInternalServerError, resp.StatusCode)
	})

	t.Run("success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})
		planetService := resttest.NewMockPlanetService(ctrl)

		planetService.EXPECT().
			Delete(gomock.Any(), gomock.Eq(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))).
			Return(nil)

		NewPlanetController(router, planetService)

		resp, err := router.Test(httptest.NewRequest(fiber.MethodDelete, "/planets/6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusNoContent, resp.StatusCode)
	})
}

func Test_errorHandle(t *testing.T) {
	t.Run("fiber error", func(t *testing.T) {
		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})

		router.Get("/", func(c *fiber.Ctx) error {
			return errorHandle(c, fiber.NewError(fiber.StatusTeapot, "error"))
		})

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusTeapot, resp.StatusCode)

		body, err := ioutil.ReadAll(resp.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, `{"code":418, "message": "error"}`, string(body))
	})

	t.Run("not found", func(t *testing.T) {
		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})

		router.Get("/", func(c *fiber.Ctx) error {
			return errorHandle(c, core.ErrPlanetNotFound)
		})

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusNotFound, resp.StatusCode)
	})

	t.Run("validation errors", func(t *testing.T) {
		router := fiber.New(fiber.Config{ReadTimeout: time.Minute, WriteTimeout: time.Minute, IdleTimeout: time.Minute})

		router.Get("/", func(c *fiber.Ctx) error {
			return errorHandle(c, &core.ValidationErrors{"error"})
		})

		resp, err := router.Test(httptest.NewRequest(fiber.MethodGet, "/", nil))
		assert.Nil(t, err)
		assert.Equal(t, fiber.StatusUnprocessableEntity, resp.StatusCode)

		body, err := ioutil.ReadAll(resp.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, `["error"]`, string(body))
	})
}
