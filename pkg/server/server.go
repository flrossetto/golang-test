package server

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"syscall"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/star-wars/api/pkg/core"
	"github.com/star-wars/api/pkg/integrations/starwarsapi"
	"github.com/star-wars/api/pkg/repository"
	"github.com/star-wars/api/pkg/repository/elastic"
	"github.com/star-wars/api/pkg/repository/mongodb"
	"github.com/star-wars/api/pkg/server/entrypoint/rest"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type StorageType int8

const (
	StorageMongoDb StorageType = iota
	StorageElasticSearch
)

func Run() {
	router := fiber.New()

	repo, closeConnection, err := createRepository(StorageMongoDb)
	// repo, closeConnection, err := createRepository(StorageElasticSearch)
	if err != nil {
		panic(err)
	}

	starWarsAPI := starwarsapi.New()

	rest.NewPlanetController(
		router, core.NewPlanetService(repo, starWarsAPI))

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)
		<-c

		router.Shutdown()
		closeConnection()
	}()

	logrus.Trace("Listen :3000")
	router.Listen(":3000")
}

func createRepository(connectionType StorageType) (repository.PlanetRepository, func(), error) {
	switch connectionType {
	case StorageMongoDb:
		client := connectMongoDB()
		repo := mongodb.NewPlanetCollection(client, "star-wars")

		disconnect := func() {
			client.Disconnect(context.TODO())
		}

		return repo, disconnect, nil

	case StorageElasticSearch:
		client := connectElasticSearch()
		repo := elastic.NewPlanetCollection(client, "star-wars")

		return repo, func() {}, nil

	default:
		return nil, nil, errors.New("unsuported type")
	}
}

func connectMongoDB() *mongo.Client {
	logrus.Trace("Open mongo connection")

	client, err := mongo.Connect(
		context.Background(),
		options.Client().ApplyURI("mongodb://127.0.0.1:27017"),
	)
	if err != nil {
		panic(err)
	}

	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		panic(err)
	}

	return client
}

func connectElasticSearch() *elasticsearch.Client {
	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://localhost:9200",
		},
	}

	client, err := elasticsearch.NewClient(cfg)
	if err != nil {
		panic(err)
	}

	if _, err := client.Info(); err != nil {
		panic(err)
	}

	return client
}
