package core

import (
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
	"github.com/google/uuid"
	"github.com/star-wars/api/pkg/repository/entity"
)

//go:generate mockgen -destination=../../mocks/coretest/starwarsapiintegrations.go -package=coretest github.com/star-wars/api/pkg/core StarWarsAPIIntegrations
type StarWarsAPIIntegrations interface {
	LoadPlanetDetails(planet *entity.Planet) error
}

//go:generate mockgen -destination=../../mocks/coretest/validate.go -package=coretest github.com/star-wars/api/pkg/core Validate
type Validate interface {
	Validate(s interface{}) error
}

type validate struct {
	trans    ut.Translator
	validate *validator.Validate
}

func newValidate() *validate {
	en := en.New()
	trans, _ := ut.New(en, en).GetTranslator("en")

	vld := validator.New()
	en_translations.RegisterDefaultTranslations(vld, trans)

	return &validate{
		trans:    trans,
		validate: vld,
	}
}

func (v *validate) Validate(s interface{}) error {
	err := v.validate.Struct(s)
	if err == nil {
		return nil
	}

	var errs ValidationErrors

	for _, e := range err.(validator.ValidationErrors) {
		errs = append(errs, e.Translate(v.trans))
	}

	return &errs
}

//go:generate mockgen -destination=../../mocks/coretest/uuidgenerate.go -package=coretest github.com/star-wars/api/pkg/core UUIDGenerate
type UUIDGenerate interface {
	New() uuid.UUID
}

type uuidGenerate struct {
	f func() uuid.UUID
}

func (g *uuidGenerate) New() uuid.UUID {
	return g.f()
}
