package core

import (
	"errors"
	"strings"
)

var (
	ErrPlanetNotFound   = errors.New("planet not found")
	ErrDuplicatedPlanet = errors.New("duplicated planet")
)

type ValidationErrors []string

func (e ValidationErrors) Error() string {
	return strings.Join(e, "; ")
}
