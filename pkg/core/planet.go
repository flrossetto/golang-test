package core

import (
	"context"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/star-wars/api/pkg/repository"
	"github.com/star-wars/api/pkg/repository/entity"
)

type (
	FindPlanetIntent struct {
		Code int64
		Name string
	}

	CreatePlanetIntent struct {
		Code int64 `validate:"required"`
	}

	PlanetFindRepresentation struct {
		ID   uuid.UUID
		Code int64
		Name string
	}

	PlanetRepresentation struct {
		ID         uuid.UUID
		Code       int64
		Name       string
		Climate    string
		Terrain    string
		FilmsCount int64
	}
)

type PlanetServiceOption func(*PlanetServiceImp)

func PlanetServiceValidate(validate Validate) PlanetServiceOption {
	return func(service *PlanetServiceImp) {
		service.validate = validate
	}
}

func PlanetServiceUUID(uuid UUIDGenerate) PlanetServiceOption {
	return func(service *PlanetServiceImp) {
		service.uuid = uuid
	}
}

type PlanetServiceImp struct {
	repo        repository.PlanetRepository
	starWarsAPI StarWarsAPIIntegrations
	validate    Validate
	uuid        UUIDGenerate
}

func NewPlanetService(repo repository.PlanetRepository, starWarsAPI StarWarsAPIIntegrations, opts ...PlanetServiceOption) *PlanetServiceImp {
	service := &PlanetServiceImp{
		repo:        repo,
		starWarsAPI: starWarsAPI,
		validate:    newValidate(),
		uuid:        &uuidGenerate{uuid.New},
	}

	for _, f := range opts {
		f(service)
	}

	return service
}

func (s *PlanetServiceImp) Find(ctx context.Context, intent FindPlanetIntent) ([]PlanetFindRepresentation, error) {
	planets, err := s.repo.Find(ctx, intent.Code, intent.Name)
	if err != nil {
		logrus.WithFields(logrus.Fields{"service": "planet", "method": "Find", "code": intent.Code, "name": intent.Name}).Error(err)
		return nil, err
	}

	result := make([]PlanetFindRepresentation, 0, len(planets))

	for _, planet := range planets {
		result = append(result, PlanetFindRepresentation{
			ID:   planet.ID,
			Code: planet.Code,
			Name: planet.Name,
		})
	}

	return result, nil
}

func (s *PlanetServiceImp) Retrieve(ctx context.Context, id uuid.UUID) (*PlanetRepresentation, error) {
	planet, err := s.repo.Retrieve(ctx, id)
	if err != nil {
		logrus.WithFields(logrus.Fields{"service": "planet", "method": "Retrieve", "planet_id": id}).Error(err)
		return nil, ErrPlanetNotFound
	}

	view := PlanetRepresentation{
		ID:         planet.ID,
		Code:       planet.Code,
		Name:       planet.Name,
		Climate:    planet.Climate,
		Terrain:    planet.Terrain,
		FilmsCount: planet.FilmsCount,
	}

	return &view, nil
}

func (s *PlanetServiceImp) Create(ctx context.Context, intent CreatePlanetIntent) error {
	if err := s.validate.Validate(intent); err != nil {
		return err
	}

	planet := entity.Planet{
		ID:   s.uuid.New(),
		Code: intent.Code,
	}

	if err := s.starWarsAPI.LoadPlanetDetails(&planet); err != nil {
		return err
	}

	return s.repo.Create(ctx, planet)
}

func (s *PlanetServiceImp) Delete(ctx context.Context, id uuid.UUID) error {
	_, err := s.repo.Retrieve(ctx, id)
	if err != nil {
		logrus.WithFields(logrus.Fields{"service": "planet", "method": "Delete", "planet_id": id}).Error(err)
		return ErrPlanetNotFound
	}

	return s.repo.Delete(ctx, id)
}
