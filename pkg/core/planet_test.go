package core

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/star-wars/api/mocks/coretest"
	"github.com/star-wars/api/mocks/repositorytest"
	"github.com/star-wars/api/pkg/repository/entity"
	"github.com/stretchr/testify/assert"
)

func TestPlanetService_Find(t *testing.T) {
	t.Run("find error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPI := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		repo.EXPECT().
			Find(gomock.Any(), gomock.Eq(int64(1)), gomock.Eq("nome")).
			Return(nil, errors.New("find error"))

		service := NewPlanetService(repo, starWarsAPI, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))
		planets, err := service.Find(context.Background(), FindPlanetIntent{Code: 1, Name: "nome"})
		assert.EqualError(t, err, "find error")
		assert.Empty(t, planets)
	})

	t.Run("success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPI := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		entityPlanets := []*entity.Planet{
			{
				ID:         uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"),
				Code:       1,
				Name:       "Name 1",
				Climate:    "Climate 1",
				Terrain:    "Terrain 1",
				FilmsCount: 1,
			},
			{
				ID:         uuid.MustParse("d65d8637-d161-4ed1-9696-3c2bead9612f"),
				Code:       1,
				Name:       "Name 2",
				Climate:    "Climate 2",
				Terrain:    "Terrain 2",
				FilmsCount: 2,
			},
		}

		repo.EXPECT().
			Find(gomock.Any(), gomock.Eq(int64(1)), gomock.Eq("nome")).
			Return(entityPlanets, nil)

		service := NewPlanetService(repo, starWarsAPI, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		planets, err := service.Find(context.Background(), FindPlanetIntent{Code: 1, Name: "nome"})
		assert.Nil(t, err)

		expected := []PlanetFindRepresentation{
			{
				ID:   uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"),
				Code: 1,
				Name: "Name 1",
			},
			{
				ID:   uuid.MustParse("d65d8637-d161-4ed1-9696-3c2bead9612f"),
				Code: 1,
				Name: "Name 2",
			},
		}
		assert.EqualValues(t, expected, planets)
	})
}

func TestPlanetService_Retrieve(t *testing.T) {
	t.Run("repository error", func(t *testing.T) {
		id := uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b")

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		repo.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(id)).
			Return(nil, errors.New("error"))

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		planet, err := service.Retrieve(context.Background(), id)
		assert.ErrorIs(t, err, ErrPlanetNotFound)
		assert.Nil(t, planet)
	})

	t.Run("success", func(t *testing.T) {
		id := uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b")

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repoPlanet := &entity.Planet{
			ID:         id,
			Code:       1,
			Name:       "Name",
			Climate:    "Climate",
			Terrain:    "Terrain",
			FilmsCount: 10,
		}

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		repo.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(id)).
			Return(repoPlanet, nil)

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		planet, err := service.Retrieve(context.Background(), id)
		assert.Nil(t, err)

		expected := &PlanetRepresentation{
			ID:         id,
			Code:       1,
			Name:       "Name",
			Climate:    "Climate",
			Terrain:    "Terrain",
			FilmsCount: 10,
		}
		assert.EqualValues(t, expected, planet)
	})
}

func TestPlanetService_Create(t *testing.T) {
	t.Run("validator error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		intent := CreatePlanetIntent{}

		validate.EXPECT().
			Validate(gomock.Eq(intent)).
			Return(errors.New("validator"))

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Create(context.Background(), intent)
		assert.EqualError(t, err, "validator")
	})

	t.Run("star wars integrations error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPI := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		intent := CreatePlanetIntent{
			Code: 1,
		}

		validate.EXPECT().
			Validate(gomock.Eq(intent)).
			Return(nil)

		uuidGenerate.EXPECT().
			New().
			Return(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))

		starWarsAPI.EXPECT().
			LoadPlanetDetails(gomock.Any()).
			Return(errors.New("integrations"))

		service := NewPlanetService(repo, starWarsAPI, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Create(context.Background(), intent)
		assert.EqualError(t, err, "integrations")
	})

	t.Run("repository error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		intent := CreatePlanetIntent{
			Code: 1,
		}

		planet := entity.Planet{
			ID:         uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"),
			Code:       1,
			Name:       "Tatooine",
			Climate:    "arid",
			Terrain:    "desert",
			FilmsCount: 5,
		}

		validate.EXPECT().
			Validate(gomock.Eq(intent)).
			Return(nil)

		starWarsAPIIntegrations.EXPECT().
			LoadPlanetDetails(gomock.Any()).
			DoAndReturn(func(planet *entity.Planet) error {
				planet.Name = "Tatooine"
				planet.Climate = "arid"
				planet.Terrain = "desert"
				planet.FilmsCount = 5
				return nil
			})

		uuidGenerate.EXPECT().
			New().
			Return(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))

		repo.EXPECT().
			Create(gomock.Any(), gomock.Eq(planet)).
			Return(errors.New("repository"))

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Create(context.Background(), intent)
		assert.EqualError(t, err, "repository")
	})

	t.Run("success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		intent := CreatePlanetIntent{
			Code: 1,
		}

		planet := entity.Planet{
			ID:         uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"),
			Code:       1,
			Name:       "Tatooine",
			Climate:    "arid",
			Terrain:    "desert",
			FilmsCount: 5,
		}

		validate.EXPECT().
			Validate(gomock.Eq(intent)).
			Return(nil)

		starWarsAPIIntegrations.EXPECT().
			LoadPlanetDetails(gomock.Any()).
			DoAndReturn(func(planet *entity.Planet) error {
				planet.Name = "Tatooine"
				planet.Climate = "arid"
				planet.Terrain = "desert"
				planet.FilmsCount = 5
				return nil
			})

		uuidGenerate.EXPECT().
			New().
			Return(uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"))

		repo.EXPECT().
			Create(gomock.Any(), gomock.Eq(planet)).
			Return(nil)

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Create(context.Background(), intent)
		assert.Nil(t, err)
	})
}

func TestPlanetService_Delete(t *testing.T) {
	t.Run("planet not found", func(t *testing.T) {
		id := uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b")

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		repo.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(id)).
			Return(nil, errors.New("error"))

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Delete(context.Background(), id)
		assert.ErrorIs(t, err, ErrPlanetNotFound)
	})

	t.Run("repository delete error", func(t *testing.T) {
		id := uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b")

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		repo.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(id)).
			Return(&entity.Planet{}, nil)

		repo.EXPECT().
			Delete(gomock.Any(), gomock.Eq(id)).
			Return(errors.New("repository"))

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Delete(context.Background(), id)
		assert.EqualError(t, err, "repository")
	})

	t.Run("success", func(t *testing.T) {
		id := uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b")

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		repo := repositorytest.NewMockPlanetRepository(ctrl)
		starWarsAPIIntegrations := coretest.NewMockStarWarsAPIIntegrations(ctrl)
		validate := coretest.NewMockValidate(ctrl)
		uuidGenerate := coretest.NewMockUUIDGenerate(ctrl)

		repo.EXPECT().
			Retrieve(gomock.Any(), gomock.Eq(id)).
			Return(&entity.Planet{}, nil)

		repo.EXPECT().
			Delete(gomock.Any(), gomock.Eq(id)).
			Return(nil)

		service := NewPlanetService(repo, starWarsAPIIntegrations, PlanetServiceValidate(validate), PlanetServiceUUID(uuidGenerate))

		err := service.Delete(context.Background(), id)
		assert.Nil(t, err)
	})
}
