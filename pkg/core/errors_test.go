package core

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidationErrors_Error(t *testing.T) {
	t.Run("empty", func(t *testing.T) {
		err := ValidationErrors{}
		assert.Equal(t, "", err.Error())
	})
	t.Run("one", func(t *testing.T) {
		err := ValidationErrors{"123"}
		assert.Equal(t, "123", err.Error())
	})
	t.Run("two", func(t *testing.T) {
		err := ValidationErrors{"123", "456"}
		assert.Equal(t, "123; 456", err.Error())
	})
	t.Run("three", func(t *testing.T) {
		err := ValidationErrors{"123", "456", "789"}
		assert.Equal(t, "123; 456; 789", err.Error())
	})
}
