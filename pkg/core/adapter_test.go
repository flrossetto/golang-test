package core

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func Test_uuidGenerate_New(t *testing.T) {
	var generate UUIDGenerate = &uuidGenerate{f: func() uuid.UUID {
		return uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b")
	}}

	assert.Equal(t, uuid.MustParse("6d5e147d-a9a9-4ee2-a7de-5a2ea0b6683b"), generate.New())
}

func Test_validate_Validate(t *testing.T) {
	t.Run("without error", func(t *testing.T) {
		value := struct {
			Name string `validate:"required"`
		}{
			Name: "Name",
		}

		validate := newValidate()

		err := validate.Validate(value)
		assert.Nil(t, err)
	})

	t.Run("with error", func(t *testing.T) {
		value := struct {
			Name string `validate:"required"`
			Age  int    `validate:"required"`
		}{}

		validate := newValidate()

		err := validate.Validate(value)
		assert.EqualValues(t, &ValidationErrors{"Name is a required field", "Age is a required field"}, err)
	})
}
